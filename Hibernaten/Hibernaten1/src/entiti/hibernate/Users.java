
package entiti.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user",catalog="users")
public class Users {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="ime")
    private String ime;
   @Column(name="adresa")
    private String adresa;
   @Column(name="godine")
    private int godine;
    @Column(name="dohodak")
    private int dohodak;
    
    
    
    public Users(){
        
    }

    public Users(String ime, String adresa, int godine, int dohodak) {
        this.ime = ime;
        this.adresa = adresa;
        this.godine = godine;
        this.dohodak = dohodak;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public int getGodine() {
        return godine;
    }

    public void setGodine(int godine) {
        this.godine = godine;
    }

    public int getDohodak() {
        return dohodak;
    }

    public void setDohodak(int dohodak) {
        this.dohodak = dohodak;
    }
    
    
    
}
