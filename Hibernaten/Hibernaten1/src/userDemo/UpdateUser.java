
package userDemo;

import entiti.hibernate.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class UpdateUser {
    
    
       public static void main(String[] args) {
           
           
        SessionFactory factory=(SessionFactory) new Configuration().configure("hibernate.cfg.xml").
                addAnnotatedClass(Users.class).buildSessionFactory();
        Session session=factory.getCurrentSession();
        try{
         int userId=10;
            
            
            
            session=factory.getCurrentSession();
            session.beginTransaction();
            System.out.println("update korisnika po Id-ju: " + userId);
            
            Users users=session.get(Users.class, userId);
            System.out.println("Update.....");
            users.setIme("Bogoljub Karic");
            session.getTransaction().commit();
            
            
            System.out.println("Uspesan update korisnika!!!");
            
        }
        finally{
            factory.close();
            
        }
    }
    
}
