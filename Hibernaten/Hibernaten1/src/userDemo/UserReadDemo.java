
package userDemo;

import entiti.hibernate.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class UserReadDemo {
    
    
      public static void main(String[] args) {
        SessionFactory factory=(SessionFactory) new Configuration().configure("hibernate.cfg.xml").
                addAnnotatedClass(Users.class).buildSessionFactory();
        Session session=factory.getCurrentSession();
        try{
            System.out.println("Ubaciti novog korisnika...");
            Users user= new Users("Uros Mitrovic","Palih boraca 28",36,52000);
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            
            System.out.println("Upamtiti korisnika: " + user.getId());
            
            session=factory.getCurrentSession();
            session.beginTransaction();
            System.out.println("nadjen korisnik po Id-ju: " + user.getId());
            
            Users users=session.get(Users.class, user.getId());
            System.out.println("Zavrseno: " + users);
            session.getTransaction().commit();
            
            
            System.out.println("Uspesno sacuvan korisnik!!!");
            
        }
        finally{
            factory.close();
            
        }
    }
    
}
