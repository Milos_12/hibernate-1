
package userDemo;

import entiti.hibernate.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteUser {
    
     public static void main(String[] args) {
           
           
        SessionFactory factory=(SessionFactory) new Configuration().configure("hibernate.cfg.xml").
                addAnnotatedClass(Users.class).buildSessionFactory();
        Session session=factory.getCurrentSession();
        try{
         int userId=21;
            
            
            
            session=factory.getCurrentSession();
            session.beginTransaction();
            System.out.println("delete korisnika po Id-ju: " + userId);
            
            Users users=session.get(Users.class, userId);
            System.out.println("User deleted: " + users);
           session.delete(users);
             session.getTransaction().commit();
            System.out.println("Uspesno obrisan  korisnik!!!");
            
        }
        finally{
            factory.close();
            
        }
    }
    
}
