
package userDemo;

import entiti.hibernate.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


 
public class UserDemo {
    
    
    
    public static void main(String[] args) {
        SessionFactory factory=(SessionFactory) new Configuration().configure("hibernate.cfg.xml").
                addAnnotatedClass(Users.class).buildSessionFactory();
        Session session=factory.getCurrentSession();
        try{
            System.out.println("Ubaciti novog korisnika...");
            Users user= new Users("Uros Djuric","Ustanicka 28",42,45000);
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            System.out.println("Uspesno sacuvan korisnik!!!");
            
        }
        finally{
            factory.close();
            
        }
    }
    
}
