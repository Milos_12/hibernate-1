package userDemo;

import entiti.hibernate.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class PrimarniKljuc {
    public static void main(String[] args) {
        
        
         SessionFactory factory=(SessionFactory) new Configuration().configure("hibernate.cfg.xml").
                addAnnotatedClass(Users.class).buildSessionFactory();
        Session session=factory.getCurrentSession();
        try{
            System.out.println("Ubaciti novog korisnika...");
            Users user1= new Users("Bratimir Markovic","Ustanicka 29",41,45600);
            session.beginTransaction();
            session.save(user1);
            session.getTransaction().commit();
            System.out.println("Uspesno sacuvan korisnik!!!");
            
        }
        finally{
            factory.close();
            
        }
    }
    }
    

